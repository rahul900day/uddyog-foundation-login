<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

// User Dashboard Routes
Route::prefix('home')->group(function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::put('/update', 'HomeController@update')->name('student_update');
    Route::post('/submit', 'HomeController@submit')->name('student_submit');
});

// Admin Dashboard Routes
Route::prefix('backend')->group(function() {
    Route::get('/', 'AdminController@index')->name('backend');
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin-login');
    Route::post('/login', 'Auth\AdminLoginController@login');
    // Students Stuff
    Route::get('/students-active', 'AdminController@student_active')->name('student-active');
    Route::get('/students-pending', 'AdminController@student_pending')->name('student-pending');
    Route::get('/student/{id}', 'AdminController@show_student');
    Route::post('/student/{id}/approve', 'AdminController@student_approve');
    // Center Stuff
    Route::get('/center-active', 'AdminController@center_active')->name('center-active');
    Route::get('/center-pending', 'AdminController@center_pending')->name('center-pending');
    Route::get('/center/{id}', 'AdminController@show_center');
    Route::post('/center/{id}/approve', 'AdminController@center_approve');
    // Courses Stuff
    Route::middleware('auth:admin')->get('/add-couse-type', 'CoursesController@showCourseTypeForm')->name('add-course-type');
    Route::middleware('auth:admin')->post('/add-couse-type', 'CoursesController@addCourseType');
    Route::middleware('auth:admin')->get('/add-couse', 'CoursesController@showAddCourseForm')->name('add-course');
    Route::middleware('auth:admin')->post('/add-couse', 'CoursesController@addCourses');
});

// Center Dashboard Routes
Route::prefix('center')->group(function() {
    Route::get('/', 'CenterController@home');
    Route::get('/register', 'Auth\CenterAuthController@showRegisterForm')->name('center-register');
    Route::post('/register', 'Auth\CenterAuthController@register');
    Route::get('/login', 'Auth\CenterAuthController@showLoginForm')->name('center-login');
    Route::post('/login', 'Auth\CenterAuthController@login');
    Route::put('/update', 'CenterController@update')->name('center-update');
    Route::post('/submit', 'CenterController@submit')->name('center-submit');

    // Student Links
    Route::get('/students-pending', 'CenterController@students_pending')->name('center-students-pending');
    Route::get('/students-active', 'CenterController@students_active')->name('center-students-active');
    Route::get('/student/{id}', 'CenterController@show_student')->name('center-show-student');

});

// Route::get('mailable', function () {
//     $user = App\User::find(1);

//     return new App\Mail\RegistrationSuccess($user);
// });
