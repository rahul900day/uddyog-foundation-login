<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Center;
use App\User;
use App\Courses;
use App\CourseType;
use App\CenterDetails;
use Carbon\Carbon;

class CenterController extends Controller
{

    private $totalDetails = 17;

    public function __construct() {
        $this->middleware('auth:center');
    }

    public function home() {
        if(count(Auth::guard('center')->user()->centerDetails) < $this->totalDetails) {
            // Institue Info
            $this->findNullRow('ins_name');
            $this->findNullRow('off_address');
            $this->findNullRow('contact_no');
            $this->findNullRow('running_time');
            $this->findNullRow('no_students');
            $this->findNullRow('running_courses');
            $this->findNullRow('lab_mat');
            $this->findNullRow('ownership');

            // Persona Info
            $this->findNullRow('f_name');
            $this->findNullRow('res_address');
            $this->findNullRow('dob');
            $this->findNullRow('qualification');
            $this->findNullRow('aadhar_no');
            $this->findNullRow('pan_no');

            // Documents
            $this->findNullRow('center_img');
            $this->findNullRow('trade_licence_img');
            $this->findNullRow('director_img');
        }
        return view('center');
    }

    // Update Details
    public function update(Request $request) {
        $this->validate($request, [
            'ins_name'          =>  'nullable|string|max:191',
            'off_address'       =>  'nullable|string|max:191',
            'contact_no'        =>  'nullable|digits:10',
            'running_time'      =>  'nullable|date',
            'no_students'       =>  'nullable|numeric|max:999999',
            'running_courses'   =>  'nullable|string|max:191',
            'lab_mat'           =>  'nullable|string|max:191',
            'ownership'         =>  'nullable|string|max:191',

            //Personal Info
            'f_name'            =>  'nullable|string|max:191',
            'res_address'       =>  'nullable|string|max:191',
            'dob'               =>  'nullable|date',
            'qualification'     =>  'nullable|string|max:52',
            'aadhar_no'         =>  'nullable|digits:12',
            'pan_no'            =>  'nullable|digits:10',

            // Documents
            'center_img'        =>  'nullable|max:150|image',
            'trade_licence_img' =>  'nullable|max:150|image',
            'director_img'      =>  'nullable|max:50|image',
            
        ]);

        update_center_details('ins_name', $request->input('ins_name'));
        update_center_details('off_address', $request->input('off_address'));
        update_center_details('contact_no', $request->input('contact_no'));
        update_center_details('running_time', $request->input('running_time'));
        update_center_details('no_students', $request->input('no_students'));
        update_center_details('running_courses', $request->input('running_courses'));
        update_center_details('lab_mat', $request->input('lab_mat'));
        update_center_details('ownership', $request->input('ownership'));

        //personal Info
        update_center_details('f_name', $request->input('f_name'));
        update_center_details('res_address', $request->input('res_address'));
        update_center_details('dob', $request->input('dob'));
        update_center_details('qualification', $request->input('qualification'));
        update_center_details('aadhar_no', $request->input('aadhar_no'));
        update_center_details('pan_no', $request->input('pan_no'));

        // Documents
        if($request->hasFile('center_img')) {
            if($request->file('center_img')->isValid()) {
                $center_img = changeFileName($request->file('center_img'));
                $request->file('center_img')->storeAs('public/documents', $center_img);

                update_center_details('center_img', $center_img);
            }
        }

        if($request->hasFile('trade_licence_img')) {
            if($request->file('trade_licence_img')->isValid()) {
                $trade_licence_img = changeFileName($request->file('trade_licence_img'));
                $request->file('trade_licence_img')->storeAs('public/documents', $trade_licence_img);

                update_center_details('trade_licence_img', $trade_licence_img);
            }
        }

        if($request->hasFile('director_img')) {
            if($request->file('director_img')->isValid()) {
                $director_img = changeFileName($request->file('director_img'));
                $request->file('director_img')->storeAs('public/documents', $director_img);

                update_center_details('director_img', $director_img);
            }
        }

        return redirect('/center')->with(['success' => 'Your informations successfully saved']);
    }

    // Submit Application
    public function submit() {

        if( count(Auth::guard('center')->user()->centerDetails->where('detail_value', null)) > 0 ) {
            return redirect('/center')->with(['error' => 'Fill The Full Form to Submit']);
        }

        $center = Center::find(Auth::guard('center')->user()->id);
        $center->submitted_at = Carbon::now();
        $center->save();

        return redirect('/center');
    }

    // Show Pending Students

    public function students_pending() {

        if(Auth::user()->confirmed_at == null) {
            return abort(401);
        }

        $users = get_center_students(Auth::guard('center')->user()->id, 'pending');

        return view('center.student_pending')->with(['users' => $users]);
    }

    // Show Active Students

    public function students_active() {
        if(Auth::user()->confirmed_at == null) {
            return abort(401);
        }
        $users = get_center_students(Auth::guard('center')->user()->id, 'active');

        return view('center.student_active')->with(['users' => $users]);
    }

    // Show Student 

    public function show_student($id) { 
        $user = User::findOrFail($id);

        $centerId = $user->UserDetails->where('detail_key', 'details_center')->first()->detail_value;

        if($user->submitted_at != null || $user->confirmed != null) {
            if($centerId != Auth::guard('center')->user()->id ) {
                return abort(401);
            } 

            $course_type = CourseType::find(get_user_details('details_course_type', null, $user->id));
            $course = Courses::find(get_user_details('details_course', null, $user->id));
            $center = Center::find(get_user_details('details_center', null, $user->id));

            return view('center.show-student')->with(['user' => $user, 'course_type' => $course_type, 'course' => $course, 'center' => $center]);
        }else {
            return abort(401);
        }
        
    }

    // All Other Helper Functions
    private function findNullRow($detail_key) {
        $center_details = Auth::guard('center')->user()->centerDetails;

        if( $center_details->where('detail_key', $detail_key)->count() < 1 ) {
            $this->fillNull($detail_key);
        }
    }

    private function fillNull($detail_key) {
        $center_details = new centerDetails;
        $center_details->detail_key = $detail_key;
        $center_details->center_id = Auth::guard('center')->user()->id;
        $center_details->save();
    }
}
