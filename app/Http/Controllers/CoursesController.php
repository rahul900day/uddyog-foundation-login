<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseType;
use App\Courses;
use App\Http\Resources\CourseType as CourseTypeResources;
use App\Http\Resources\Courses as CoursesResources;

class CoursesController extends Controller
{
    // Show CourseType Add Form
    public function showCourseTypeForm() {
        $course_types = CourseType::all();
        return view('backend.add-course-type')->with(['course_types' => $course_types]);
    }

    // Add Course Type
    public function addCourseType (Request $request) {
        $this->validate( $request, [
            'type_name' => 'string|required|unique:course_type,type',
        ]);

        $course_type = new CourseType();
        $course_type->type = $request->input('type_name');
        $course_type->save();

        return redirect(route('add-course-type'));
    }

    public function getCourseType(Request $request) {

        if($request->isMethod('get')) {
            return CourseTypeResources::collection(CourseType::all());
        }else {
            $term = $request->input('term');
            $course_type = CourseType::where('type', 'LIKE', '%'.$term.'%')->get();
            return CourseTypeResources::collection($course_type);
        }
    }

    public function getApiCourses($id) {
        $course_type_id = $id;
        $courses = Courses::where('course_type_id', '=', $course_type_id)->get();
        
        return CoursesResources::collection($courses);
    }

    public function showAddCourseForm() {
        $course_types = CourseType::all();
        $courses = Courses::all();
        return view('backend.add-course')->with(['course_types' => $course_types, 'courses' => $courses]);
    }

    public function addCourses(Request $request) {
        $this->validate($request, [
            'course_name' => 'string|required',
            'course_parent' => 'required|exists:course_type,type',
        ]);

        $course_type_id = CourseType::where('type', $request->input('course_parent'))->first()->id;
        
        $course = new Courses();
        $course->course_name = $request->input('course_name');
        $course->course_type_id = $course_type_id;
        $course->save();

        return redirect(route('add-course'));
    }

}
