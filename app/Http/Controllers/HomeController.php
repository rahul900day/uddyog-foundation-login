<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\UserDetails;
use App\Center;
use App\CourseType;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationSuccess;

class HomeController extends Controller
{
    private $min_details_row = 18;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $centers = Center::where('confirmed_at', '!=', null)
                        ->where('submitted_at', '!=', null)
                        ->get();
        
        $course_types = CourseType::all();

        if(count(Auth::user()->userDetails) < $this->min_details_row) {
            // All Student Details Add a Null Row
            $this->findNullRow('details_f_name');
            $this->findNullRow('details_m_name');
            $this->findNullRow('details_ph_no');
            $this->findNullRow('details_dob');
            $this->findNullRow('details_gender');
            $this->findNullRow('details_qualification');
            $this->findNullRow('details_religion');
            $this->findNullRow('details_address');
            $this->findNullRow('details_postcode');
            $this->findNullRow('details_center');
            $this->findNullRow('details_course_type');
            $this->findNullRow('details_course');
            $this->findNullRow('details_city');
            $this->findNullRow('details_state');
            $this->findNullRow('details_aadhar_no');
            $this->findNullRow('details_image');
            $this->findNullRow('details_marksheet');
            $this->findNullRow('details_bio_data');

            return view('home')->with(['centers' => $centers, 'course_types' => $course_types]);
        }else {
            return view('home')->with(['centers' => $centers, 'course_types' => $course_types]);
        }
    }

    // Update Route
    public function update(Request $request) {
        $this->validate($request, [
            'f_name'        => 'nullable|string|max:191',
            'm_name'        => 'nullable|string|max:191',
            'ph_no'         => 'nullable|digits:10',
            'dob'           => 'nullable|date',
            'gender'        => 'nullable|in:male,female',
            'qualification' => 'nullable|string|max:30',
            'religion'      => 'nullable|string|max:20',
            'address'       => 'nullable|string',
            'post_code'     => 'nullable|digits:6',
            'center'        => 'nullable|exists:centers,id',
            'course_type'   => 'required|exists:course_type,id',
            'course'        => 'required|exists:courses,id|checkCourse:course_type',
            'state'         => 'nullable|string',
            'city'          => 'nullable|string',
            'aadhar_no'     => 'nullable|digits:12',
            'profile_image' => 'nullable|max:50|image',
            'marksheet'     => 'nullable|max:250|image',
            'bio_data'      => 'nullable|max:400|mimes:jpeg,bmp,png,doc,docx',

        ]);

        update_user_details('details_f_name', $request->input('f_name'));
        update_user_details('details_m_name', $request->input('m_name'));
        update_user_details('details_ph_no', $request->input('ph_no'));
        update_user_details('details_dob', $request->input('dob'));
        update_user_details('details_gender', $request->input('gender'));
        update_user_details('details_qualification', $request->input('qualification'));
        update_user_details('details_religion', $request->input('religion'));
        update_user_details('details_address', $request->input('address'));
        update_user_details('details_state', $request->input('state'));
        update_user_details('details_city', $request->input('city'));
        update_user_details('details_postcode', $request->input('post_code'));
        update_user_details('details_center', $request->input('center'));
        update_user_details('details_course_type', $request->input('course_type'));
        update_user_details('details_course', $request->input('course'));
        update_user_details('details_aadhar_no', $request->input('aadhar_no'));

        if($request->hasFile('profile_image')) {
            if($request->file('profile_image')->isValid()) {
                $profile_image = changeFileName($request->file('profile_image'));
                $request->file('profile_image')->storeAs('public/profile_img', $profile_image);

                update_user_details('details_image', $profile_image);
            }
        }

        if($request->hasFile('marksheet')) {
            if($request->file('marksheet')->isValid()) {
                $marksheet = changeFileName($request->file('marksheet'));
                $request->file('marksheet')->storeAs('public/documents', $marksheet);

                update_user_details('details_marksheet', $marksheet);
            }
        }

        if($request->hasFile('bio_data')) {
            if($request->file('bio_data')->isValid()) {
                $bio_data = changeFileName($request->file('bio_data'));
                $request->file('bio_data')->storeAs('public/documents', $bio_data);

                update_user_details('details_bio_data', $bio_data);
            }
        }

        return redirect('/home')->with(['success' => 'Your informations successfully saved']);
    }

    // Submit Application
    public function submit() {
        if( count(Auth::user()->userDetails->where('detail_value', null)) > 0 ) {
            return redirect('/home')->with(['error' => 'Fill The Full Form to Submit']);
        }

        $user = User::find(Auth::user()->id);
        $user->submitted_at = Carbon::now();
        $user->save();

        Mail::to($user)->send(new RegistrationSuccess($user));

        return redirect('/home');
    }

    // All Other Helper Functions
    private function findNullRow($detail_key) {
        $user_details = Auth::user()->userDetails;

        if( $user_details->where('detail_key', $detail_key)->count() < 1 ) {
            $this->fillNull($detail_key);
        }
    }

    private function fillNull($detail_key) {
        $user_details = new UserDetails;
        $user_details->detail_key = $detail_key;
        $user_details->user_id = Auth::user()->id;
        $user_details->save();
    }
}
