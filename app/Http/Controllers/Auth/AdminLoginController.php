<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    public function __construct() {
        $this->middleware('guest:admin');
    }

    public function showLoginForm() {
        return view('auth.admin-login');
    }

    public function login(Request $request) {
        $this->validate($request, [
            'email' =>  'required|email',
            'password'  =>  'required|min:8'
        ]);

        if(Auth::guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $request->input('remember'))) {
            return redirect()->intended(route('backend'));
        }

        return redirect()->back()->withInput(['email' => $request->input('email'), 'password' => $request->input('password')]);

    }
}
