<?php

namespace App\Http\Controllers\Auth;

use App\Center;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class CenterAuthController extends Controller
{

    public function showRegisterForm() {
        return view('auth.center-register');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:centers'],
            'uuid'  =>  ['required', 'string', 'max:31', 'unique:centers,uuid', 'regex:/^[\w]+$/'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    public function register(Request $request) {
        $this->validator($request->all())->validate();
        $center = Center::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password'  =>  Hash::make($request['password']),
            'uuid'  =>  $request['uuid'],
        ]);

        Auth::guard('center')->login($center);

        return redirect()->intended('center');
    }


    // Login Area
    public function showLoginForm() {
        return view('auth.center-login');
    }

    public function login(Request $request) {
        $this->validate($request, [
            'email' =>  'required|email',
            'password'  =>  'required|min:8'
        ]);

        if(Auth::guard('center')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $request->input('remember'))) {
            return redirect()->intended('center');
        }

        return redirect()->back()->withInput(['email' => $request->input('email'), 'password' => $request->input('password')]);

    }
}
