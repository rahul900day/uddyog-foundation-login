<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Center;
use App\CourseType;
use App\Courses;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('backend');
    }

    public function student_active() {
        $users = User::where('confirmed_at', '!=', null)
                        ->where('submitted_at', '!=', null)
                        ->get();

        return view('backend.student_active')->with(['users' => $users]);
    }

    public function student_pending() {
        $users = User::where('submitted_at', '!=', null)
                        ->where('confirmed_at', null)
                        ->get();

        return view('backend.student_pending')->with(['users' => $users]);
    }

    public function show_student($id) {
        $user = User::findOrFail($id);
        $course_type = CourseType::find(get_user_details('details_course_type', null, $user->id));
        $course = Courses::find(get_user_details('details_course', null, $user->id));
        $center = Center::find(get_user_details('details_center', null, $user->id));

        return view('backend.show-student')->with(['user' => $user, 'course_type' => $course_type, 'course' => $course, 'center' => $center]);
    }

    public function student_approve($id) {
        $user = User::find($id);
        $user->confirmed_at = Carbon::now();
        $user->save();

        return redirect('/backend/student/'.$id);
    }

    public function center_active() {
        $centers = Center::where('confirmed_at', '!=', null)
                        ->where('submitted_at', '!=', null)
                        ->get();

        return view('backend.center_active')->with(['centers' => $centers]);
    }

    public function center_pending() {
        $centers = Center::where('submitted_at', '!=', null)
                        ->where('confirmed_at', null)
                        ->get();

        return view('backend.center_pending')->with(['centers' => $centers]);
    }

    public function show_center($id) {
        $center = Center::findOrFail($id);

        return view('backend.show-center')->with(['center' => $center]);
    }

    public function center_approve($id) {
        $center = Center::find($id);
        $center->confirmed_at = Carbon::now();
        $center->save();

        return redirect('/backend/center/'.$id);
    }
}
