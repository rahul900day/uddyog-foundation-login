<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $table = 'courses';
    public $timestamps = false;

    public function courseType() {
        return $this->belongsTo('App\CourseType');
    }
}
