<?php 

use Illuminate\Database\Eloquent\Builder;

function get_user_details($details, $validation = null, $user_id = null, $default_return = null) {

    if($validation != null) {
        if( old($validation) != null ) {
            return old($validation);
        }
    }

    $user_id = $user_id == null ? Auth::user()->id : $user_id;
    $user = App\User::find($user_id);

    if(count($user->userDetails->where('detail_key', $details)) > 0 ) {
        $u_detail = $user->userDetails->where('detail_key', $details)->first()->detail_value;
        
        if($u_detail == null) {
            return $default_return;
        }else {
            return $u_detail;
        }
    }else {
        return $default_return;
    }
}

function get_center_details($details, $validation = null, $center_id = null, $default_return = null) {

    if($validation != null) {
        if( old($validation) != null ) {
            return old($validation);
        }
    }

    $center_id = $center_id == null ? Auth::guard('center')->user()->id : $center_id;
    $center = App\Center::find($center_id);

    if(count($center->centerDetails->where('detail_key', $details)) > 0 ) {
        $c_detail = $center->centerDetails->where('detail_key', $details)->first()->detail_value;
        
        if($c_detail == null) {
            return $default_return;
        }else {
            return $c_detail;
        }
    }else {
        return $default_return;
    }
}

function update_user_details($detail, $new_detail, $user_id = null) {

    $id = $user_id == null ? Auth::user()->id : $user_id;
    
    App\UserDetails::where('user_id', $id)
                    ->where('detail_key', $detail)
                    ->update(['detail_value' => $new_detail]);
    return true;
}

function update_center_details($detail, $new_detail, $center_id = null) {

    $id = $center_id == null ? Auth::guard('center')->user()->id : $center_id;
    
    App\CenterDetails::where('center_id', $id)
                    ->where('detail_key', $detail)
                    ->update(['detail_value' => $new_detail]);
    return true;
}

// Only supported for Database Timestapm format
function format_date($time, $format = 'd M Y g:i A') {
    $timestamp = strtotime($time);
    return date($format, $timestamp);
}

function changeFileName($file) {
    $fileWithExt = $file->getClientOriginalName();
    $fileName = pathinfo($fileWithExt, PATHINFO_FILENAME);
    $fileExt = $file->extension();
    $fileNewName = preg_replace('/[\s]+/', '_', $fileName);

    return $fileNewName.''.time().'.'.$fileExt;
}

function get_center_students($center_id, $status = null) {

    if($status == 'active' || $status == null) {
        $users = App\User::where('confirmed_at', '!=', null)
                    ->whereHas('userDetails', function ($query) use ($center_id) {
                        $query->where('detail_key', 'details_center')
                            ->where('detail_value', $center_id);
                    })
                    ->get();
    }elseif($status == 'pending') {
        $users = App\User::where('confirmed_at', '=', null)
                        ->where('submitted_at', '!=', null)
                        ->whereHas('userDetails', function ($query) use ($center_id) {
                            $query->where('detail_key', 'details_center')
                                ->where('detail_value', $center_id);
                        })
                        ->get();
    }

    return $users;
}

?>