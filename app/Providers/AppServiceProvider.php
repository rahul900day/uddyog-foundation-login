<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Custom Validation Rules
        Validator::extend('checkCourse', function($attribute, $value, $parameters, $validator) {
            $course_type_id = $validator->getData()[$parameters[0]];
            $course = DB::table('courses')->where('id', $value)->first();

            if($course->course_type_id == $course_type_id) {
                return true;
            }else {
                return false;
            }
            
        });
    }
}
