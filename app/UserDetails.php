<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    protected $table = 'student_details';

    public function users () {
        return $this->belongsTo('App\User');
    }

    public $timestamps = false;
}
