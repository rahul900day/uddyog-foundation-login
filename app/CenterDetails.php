<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CenterDetails extends Model
{
    protected $table = 'center_details';
    public $timestamps = false;

    public function center() {
        return $this->belongsTo('App\Center');
    }
}
