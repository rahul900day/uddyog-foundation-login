$(".select2").select2({
    width: '100%',
    placeholder: "Select an Item",
});

$('#course_type').select2({
    width: '100%',
    placeholder: "Select an Item",
    ajax: {
        url: '/api/course-type',
        dataType: 'json',
        processResults: function (data) {
            return {
              results: data.data
            };
          }
      }
});

function getCourse() {
    var courseTypeID = $( "#course_type option:selected" ).attr('value');
    $('#course').select2({
        width: '100%',
        placeholder: "Select an Item",
        ajax: {
            url: '/api/courses/'+courseTypeID,
            dataType: 'json',
            processResults: function (data) {
                return {
                  results: data.data
                };
              }
          }
    });
};

getCourse();

$("#course_type").change(function() {
    $('#course').children('option').remove();
    getCourse();
});