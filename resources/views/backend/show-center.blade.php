@extends('layouts.dashboard')

@section('content')
    @include('inc.admin-sidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
      
      </div>
      <!-- /.content-header -->
  
      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
              <?php
                $default_return = 'N/A';
              ?>
            <div class="col-12">
            <div class="card card-dark">
              <div class="card-header">
                <div class="d-flex justify-content-between">
                    <h1 class="m-0 card-title">{{ get_center_details('ins_name', null, $center->id) }}</h1>
                    @if($center->confirmed_at == null)
                      <p class="text-warning m-0">Pending</p>
                    @else
                      <p class="text-success m-0">Active</p>
                    @endif
                </div>
              </div>
              <div class="card-body">
                  <p><strong>Applicant Name:</strong> {{ $center->name }}</p>
                  <p><strong>Office Address:</strong> {{ get_center_details('off_address', null, $center->id, $default_return) }}</p>
                  <p><strong>Phone No.:</strong> {{ get_center_details('contact_no', null, $center->id), $default_return }}</p>
                  <p><strong>Running Since:</strong> {{ format_date(get_center_details('running_time', null, $center->id, $default_return), 'd M Y') }}</p>
                  <p><strong>Student Strength:</strong> {{ get_center_details('no_students', null, $center->id, $default_return) }}</p>
                  <p><strong>Running Courses:</strong> {{ get_center_details('running_courses', null, $center->id, $default_return) }}</p>
                  <p><strong>Lab Materials:</strong> {{ get_center_details('lab_mat', null, $center->id, $default_return) }}</p>
                  <p><strong>Ownership/Partnership Details:</strong> {{ get_center_details('ownership', null, $center->id, $default_return) }}</p>

                  <h2>Pesonal Information</h2>
                  <p><strong>Father's Name:</strong> {{ get_center_details('f_name', null, $center->id, $default_return) }}</p>
                  <p><strong>Residential Address:</strong> {{ get_center_details('res_address', null, $center->id, $default_return) }}</p>
                  <p><strong>Date Of Birth:</strong> {{ format_date(get_center_details('ownership', null, $center->id, $default_return), 'd M Y') }}</p>
                  <p><strong>Last Qualification:</strong> {{ get_center_details('qualification', null, $center->id, $default_return) }}</p>
                  <p><strong>Aadhar No:</strong> {{ get_center_details('aadhar_no', null, $center->id, $default_return) }}</p>
                  <p><strong>Pan No:</strong> {{ get_center_details('pan_no', null, $center->id, $default_return) }}</p>

                  <p><strong>Director Image:</strong></p>
                  <img src="{{ url('/') }}/storage/documents/{{ get_center_details('director_img', null, $center->id) }}">

                  <div class="clearfix mt-4">
                      <a href="{{ url('/') }}/storage/documents/{{ get_center_details('trade_licence_img', null, $center->id) }}" class="btn btn-outline-success btn-flat mr-2">Download Trade Licence</a>
                      <a href="{{ url('/') }}/storage/documents/{{ get_center_details('center_img', null, $center->id) }}" class="btn btn-outline-success btn-flat">Download Center Photo</a>
                  </div>
              </div>
              <div class="card-footer">
                  @if($center->confirmed_at == null)
                    <button class="btn btn-success btn-flat" onclick="event.preventDefault();
                    document.getElementById('approve-form').submit();">Approve</button>

                    <form id="approve-form" action="{{ url('/backend/center/'.$center->id.'/approve') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                  @endif
              </div>
            </div><!-- /.Card -->
            </div><!-- /.col-12 -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('inc.dashboard-footer')
@endsection
