@extends('layouts.dashboard')

@section('content')
    @include('inc.admin-sidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Add Course</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
              <div class="col-12">
                  <div class="card">
                      <div class="card-body">
                          <form action="{{ route('add-course') }}" method="post">
                              @csrf
                              <div class="row form-group">
                                    <div class="col-md-8">
                                        <input class="form-control" type="text" name="course_name" id="course_name" placeholder="Course Name" required>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="select2" name="course_parent" id="course_parent" required> 
                                            @foreach ($course_types as $course_type)
                                                <option value="{{ $course_type->type }}">{{ $course_type->type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-flat">Add</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Your Couses</h1>
            </div><!-- /.col -->
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-body p-0">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Course Name</th>
                                    <th>Course Type</th>
                                </tr>
                                @foreach ($courses as $course)
                                    <tr>
                                        <td>{{ $course->course_name }}</td>
                                        <td>{{ $course->courseType->type }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
  
      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('inc.dashboard-footer')
@endsection
