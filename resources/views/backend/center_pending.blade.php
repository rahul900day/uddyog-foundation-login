@extends('layouts.dashboard')

@section('content')
    @include('inc.admin-sidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">List Of Pending Institutes</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
              <div class="col-12">
                  <div class="card">
                      <div class="card-header">
                          <h3 class="card-title">Institues</h3>
                      </div>
                      <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <th>UUID</th>
                                    <th>Name Of Institutes</th>
                                    <th>Submitted At</th>
                                    <th>View</th>
                                </tr>

                                @foreach ($centers as $center)
                                    <tr>
                                        <td>{{ $center->id }}</td>
                                        <td>{{ $center->uuid }}</td>
                                        <td>{{ get_center_details('ins_name', null, $center->id) }}</td>
                                        <td>{{ format_date($center->submitted_at) }}</td>
                                        <td><a href="{{ url('/backend/center/'.$center->id) }}"><i class="fas fa-eye"></i></a></td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                      </div>
                  </div>
              </div>
          </div>
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
  
      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('inc.dashboard-footer')
@endsection
