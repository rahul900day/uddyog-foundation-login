@extends('layouts.dashboard')

@section('content')
    @include('inc.admin-sidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Add Course Type</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
              <div class="col-12">
                  <div class="card">
                      <div class="card-body">
                          <form action="" method="post">
                              @csrf
                              <div class="input-group input-group-md">
                                  <input class="form-control" type="text" name="type_name" id="type_name" placeholder="Course Type" required>
                                  <span class="input-group-append">
                                      <button type="submit" class="btn btn-success btn-flat">Add</button>
                                    </span>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Your Couse Types</h1>
            </div><!-- /.col -->
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-body p-0">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Types</th>
                                </tr>
                                @foreach ($course_types as $course_type)
                                    <tr>
                                        <td>{{ $course_type->type }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
  
      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('inc.dashboard-footer')
@endsection
