@extends('layouts.dashboard')

@section('content')
    @include('inc.admin-sidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
      </div>
      <!-- /.content-header -->
  
      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
              <div class="col-12">
                <div class="card card-dark">
                    {{-- Card Header --}}
                    <div class="card-header">
                      <div class="d-flex justify-content-between">
                          <h1 class="m-0 card-title">{{ $user->name }}</h1>
                          @if($user->confirmed_at == null)
                            <p class="text-warning m-0">Pending</p>
                          @else
                            <p class="text-success m-0">Active</p>
                          @endif
                      </div>
                    </div>
                    {{-- /Card Header --}}
                    <?php
                        $default_return = 'Not Specified';
                    ?>
                    {{-- Card Body --}}
                    <div class="card-body">
                        <p><strong>Father's Name:</strong> {{ get_user_details('details_f_name', null, $user->id, $default_return) }}</p>
                        <p><strong>Mothers's Name:</strong> {{ get_user_details('details_m_name', null, $user->id, $default_return) }}</p>
                        <p><strong>phone Number:</strong> {{ get_user_details('details_ph_no', null, $user->id, $default_return) }}</p>
                        <p><strong>Date Of Birth:</strong> {{ format_date(get_user_details('details_dob', null, $user->id, $default_return), 'd M Y') }}</p>
                        <p><strong>Gender:</strong> {{ get_user_details('details_gender', null, $user->id, $default_return) }}</p>
                        <p><strong>Qualification:</strong> {{ get_user_details('details_qualification', null, $user->id, $default_return) }}</p>
                        <p><strong>Religion:</strong> {{ get_user_details('details_religion', null, $user->id, $default_return) }}</p>
                        <p><strong>Address:</strong> {{ get_user_details('details_address', null, $user->id, $default_return) }}</p>
                        <p><strong>Pincode:</strong> {{ get_user_details('details_postcode', null, $user->id, $default_return) }}</p>
                        <p><strong>Institute:</strong> {{ get_center_details('ins_name', null, $center->id, $default_return) }}</p>
                        <p><strong>Course Type:</strong> {{ $course_type->type }}</p>
                        <p><strong>Course Name:</strong> {{ $course->course_name }}</p>
                        <p><strong>City:</strong> {{ get_user_details('details_city', null, $user->id, $default_return) }}</p>
                        <p><strong>State:</strong> {{ get_user_details('details_state', null, $user->id, $default_return) }}</p>

                        <p><strong>Image</strong></p>
                        <img src="{{ url('/') }}/storage/profile_img/{{ get_user_details('details_image', null, $user->id) }}">
                        <div class="clearfix mt-4">
                            <a href="{{ url('/') }}/storage/documents/{{ get_user_details('details_marksheet', null, $user->id) }}" class="btn btn-outline-success btn-flat mr-2">Download Marksheet</a>
                            <a href="{{ url('/') }}/storage/documents/{{ get_user_details('details_bio_data', null, $user->id) }}" class="btn btn-outline-success btn-flat">Download Bio Data</a>
                        </div>
                    </div>
                    {{-- /Card Body --}}
                    <div class="card-footer">
                        @if($user->confirmed_at == null)
                          <button class="btn btn-success btn-flat" onclick="event.preventDefault(); document.getElementById('approve-form').submit();">Approve</button>
                          <form id="approve-form" action="{{ url('/backend/student/'.$user->id.'/approve') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                        @endif
                    </div>
                </div>
              </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('inc.dashboard-footer')
@endsection
