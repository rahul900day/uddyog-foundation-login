@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h1>Welcome To Uddyog Foundation Students Portal</h1>
                <p>Choose Your Application Method</p>
                <a href="" class="btn btn-secondary ">Offline Method</a>
                <a href="{{ route('register') }}" class="btn btn-outline-dark">Online Mehod</a>
            </div>
        </div>
    </div>
@endsection