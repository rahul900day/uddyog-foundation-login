@component('mail::message')
# Hello {{ $user->name }},

You have successfully submitted your application form to **_Uddyog Foundation Association_**

### Your Student Id is: {{ $user->id }}
### Your Registration Time: {{ format_date($user->submitted_at) }}

Regards,<br>
Uddyog Foundation Association
@endcomponent
