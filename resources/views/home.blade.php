@extends('layouts.dashboard')

@section('content')
    @include('inc.student-sidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      @include('inc.user-alert')
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Details</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
  
      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-md-10">
              @if(Auth::user()->submitted_at == null)
              <div class="card card-primary">
                {{-- Header --}}
                <div class="card-header">
                  <h3 class="card-title">Fill Up Your Details</h3>
                </div>
                {{-- Form --}}
                <form action="{{ route('student_update') }}" method="post" role="form" accept-charset="UTF-8" enctype="multipart/form-data">
                  @csrf
                  {{-- Card Body --}}
                  <div class="card-body">
                    {{-- Father's Name --}}
                    <div class="form-group">
                      <label for="f_name">Father's Name</label>
                      <input type="text" name="f_name" id="f_name" class="form-control" maxlength="191" value="{{ get_user_details('details_f_name', 'f_name') }}">
                    </div>
                    {{-- Mother's Name --}}
                    <div class="form-group">
                      <label for="m_name">Mother's Name</label>
                      <input type="text" name="m_name" id="m_name" class="form-control" maxlength="191" value="{{ get_user_details('details_m_name', 'm_name') }}">
                    </div>
                    {{-- Mobile No --}}
                    <div class="form-group">
                      <label for="ph_no">Mobile Number</label><br>
                      <small class="text-muted">(10 Digit Mobile Number)</small>
                      <input type="tel" name="ph_no" id="ph_no" class="form-control" maxlength="10" value="{{ get_user_details('details_ph_no', 'ph_no') }}">
                    </div>
                    {{-- Date Of Birth --}}
                    <div class="form-group">
                      <label for="dob">Date Of Birth</label>
                      <input type="date" name="dob" id="dob" class="form-control" value="{{ get_user_details('details_dob', 'dob') }}">
                    </div>
                    {{-- Gender --}}
                    <div class="form-group">
                      <label for="gender">Gender</label><br>
                      <input type="radio" name="gender" id="gender_male" value="male" class="mr-2" @if(get_user_details('details_gender', 'gender') == 'male' ) checked="checked" @endif ><label for="gender_male">Male</label><br>
                      <input type="radio" name="gender" id="gender_female" value="female" class="mr-2" @if(get_user_details('details_gender', 'gender') == 'female' ) checked="checked" @endif><label for="gender_female">Female</label>
                    </div>
                    {{-- Qualification --}}
                    <div class="form-group">
                      <label for="qualification">Your Last Qualification</label>
                      <input type="text" name="qualification" id="qualification" class="form-control" maxlength="30" value="{{ get_user_details('details_qualification', 'qualification') }}">
                    </div>
                    {{-- Center --}}
                    <div class="form-group">
                        <label for="center">Select Institutes</label><br>
                        <small class="text-muted">(Select which institute you want your admission)</small>
                        <select class="select2" name="center" id="center">
                            @foreach ($centers as $center)
                              <option value="{{ $center->id }}">{{ get_center_details('ins_name', null, $center->id) }} ({{ $center->uuid }})</option>
                            @endforeach
                        </select>
                      </div>
                      {{-- Course Type --}}
                      <div class="form-group">
                        <label for="course_type">Select Course Type</label><br>
                        <select class="select2" name="course_type" id="course_type" required></select>
                      </div>
                      {{-- Course --}}
                      <div class="form-group">
                        <label for="course">Select Course</label><br>
                        <select name="course" id="course" required></select>
                      </div>
                      {{-- Religion --}}
                      <div class="form-group">
                        <label for="address">Religion</label><br>
                        <small class="text-muted">(IE: Hindu, Muslim, Others)</small>
                        <input type="text" name="religion" id="religion" class="form-control" maxlength="30" value="{{ get_user_details('details_religion', 'religion') }}">
                      </div>
                      {{-- Address --}}
                      <div class="form-group">
                        <label for="address">Address</label><br>
                        <small class="text-muted">(Street Address & Area)</small>
                        <input type="text" name="address" id="address" class="form-control" maxlength="30" value="{{ get_user_details('details_address', 'address') }}">
                      </div>
                      {{-- State --}}
                      <div class="form-group">
                        <label for="state">State</label>
                        <input type="text" name="state" id="state" class="form-control" value="{{ get_user_details('details_state', 'state') }}">
                      </div>
                      {{-- City --}}
                      <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" name="city" id="city" class="form-control" value="{{ get_user_details('details_city', 'city') }}">
                      </div>
                      {{-- Postal Code --}}
                      <div class="form-group">
                        <label for="post_code">Pincode</label>
                        <input type="text" name="post_code" id="post_code" class="form-control" maxlength="6" value="{{ get_user_details('details_postcode', 'post_code') }}">
                      </div>
                      {{-- Aadhar No --}}
                      <div class="form-group">
                        <label for="aadhar_no">Aadhar No</label>
                        <input type="text" name="aadhar_no" id="aadhar_no" class="form-control" maxlength="12" value="{{ get_user_details('details_aadhar_no', 'aadhar_no') }}">
                      </div>
                      {{-- Profile Image --}}
                      <div class="form-group">
                        <div class="custom-file">
                            <input type="file" name="profile_image" id="profile_image" class="form-control">
                            <label for="profile_image" class="custom-file-label">Upload / Update Image (Maximum File Size 50 KB)</label>
                        </div>
                      </div>
                      {{-- Marksheet --}}
                      <div class="form-group">
                        <div class="custom-file">
                            <input type="file" name="marksheet" id="marksheet" class="form-control">
                            <label for="marksheet" class="custom-file-label"><strong>Upload / Update Marksheet (Image Format, Maximum File Size 250 KB)<strong></label>
                        </div>
                      </div>
                      {{-- Bio Data --}}
                      <div class="form-group">
                        <div class="custom-file">
                            <input type="file" name="bio_data" id="bio_data" class="form-control">
                            <label for="bio_data" class="custom-file-label"><strong>Upload / Update Bio Data (Image or MS Word Format, Maximum File Size 400 KB)<strong></label>
                        </div>
                      </div>
                      {{-- From Mthod --}}
                      <input type="hidden" name="_method" value="PUT">
                  </div>
                  {{-- /Card Body --}}

                  {{-- Card Footer --}}
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary btn-flat">Save Changes</button>
                    </div>
                  {{-- /Card Footer --}}
                </form>
              </div>
              @else
                <div class="callout callout-warning">
                  <h5>Form Submitted</h5>

                  <p>You have submitted your form so you are not able to edit your form again</p>
                </div>
              @endif
            </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('inc.dashboard-footer')
@endsection
