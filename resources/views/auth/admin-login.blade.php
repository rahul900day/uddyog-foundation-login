@extends('layouts.login-header')

@section('content')
{{-- Login Box --}}
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('/') }}"><img src="{{ url('/') }}/images/logo-small.png" style="max-width: 90px"></a>
    </div>

    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">SignIn With Your Admin Login Details</p>

            <form method="POST" action="{{ route('admin-login') }}">
                @csrf
                <div class="input-group mb-3">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="row">
                    <div class="col-8">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>

                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            {{ __('Login') }}
                        </button>
                    </div>
                </div>
            </form>

            <p class="mb-1">
                <a class="btn btn-link" href="{{ route('password.request') }}">I forgot my password</a>
            </p>
        </div>
    </div>
</div>
@endsection
