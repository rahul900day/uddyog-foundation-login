@if(count($errors) > 0 || session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> Alert!</h5>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach

                @if(session('error'))
                    <li>{{ session('error') }}</li>
                @endif

            </ul>
    </div>
@endif