<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/') }}" class="brand-link">
      <img src="{{ url('/') }}/images/logo-small.png" alt="Uddyog Foundation Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Uddyog Foundation</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ url('/') }}/images/avatar.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a class="d-block">{{ Auth::guard('admin')->user()->name }} <span class="text-success">(ADMIN)</span></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                    Students
                    </p>
                </a>

                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('student-active') }}" class="nav-link">
                            <i class="nav-icon far fa-circle"></i>
                            <p>
                            Active
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('student-pending') }}" class="nav-link">
                            <i class="nav-icon far fa-circle"></i>
                            <p>
                            Pending
                            </p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-university"></i>
                    <p>
                    Institues
                    </p>
                </a>

                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('center-active') }}" class="nav-link">
                            <i class="nav-icon far fa-circle"></i>
                            <p>
                            Active
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('center-pending') }}" class="nav-link">
                            <i class="nav-icon far fa-circle"></i>
                            <p>
                            Pending
                            </p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-book"></i>
                    <p>
                    Courses
                    </p>
                </a>

                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('add-course-type') }}" class="nav-link">
                            <i class="nav-icon far fa-circle"></i>
                            <p>
                            Course Type
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add-course') }}" class="nav-link">
                            <i class="nav-icon far fa-circle"></i>
                            <p>
                            Course
                            </p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-lock"></i>
                <p>
                  Security
                </p>
              </a>
            </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>