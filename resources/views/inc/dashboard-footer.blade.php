<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Uddyog Foundation Association
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; Designed and Developed by <a href="https://thinktoshare.com/" target="_blank">Think To Share</a>.</strong> All rights reserved.
</footer>