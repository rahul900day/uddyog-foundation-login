@if(Auth::user()->submitted_at == null)
    <div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
        <p style="color: #fff">You Have Not Submitted Your Application Yet!</p>
        <a class="btn btn-default btn-sm submit-alert-btn" onclick="event.preventDefault();
        document.getElementById('submit-form').submit();"><i class="fas fa-check"></i> Submit Now</a>

        <form id="submit-form" action="{{ route('student_submit') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
@endif

@if(count($errors) > 0 || session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> Alert!</h5>
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach

                @if(session('error'))
                    <li>{{ session('error') }}</li>
                @endif
            </ul>
        
    </div>
@endif