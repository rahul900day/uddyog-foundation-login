@extends('layouts.dashboard')

@section('content')
    @include('inc.center-sidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      @include('inc.center-alert')
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Details</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
  
      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-md-12">
              @if(Auth::user()->submitted_at == null)
              <div class="card card-danger">
                {{-- Header --}}
                <div class="card-header">
                  <h3 class="card-title">Fill The Details For New Institute</h3>
                </div>
                {{-- Form --}}
                <form action="{{ route('center-update') }}" method="post" role="form" accept-charset="UTF-8" enctype="multipart/form-data">
                  @csrf
                  {{-- Card Body --}}
                  <div class="card-body">
                    <div class="form-group">
                        <label for="ins_name">Institue Name</label>
                        <input type="text" name="ins_name" id="ins_name" class="form-control" maxlength="191" value="{{ get_center_details('ins_name', 'ins_name') }}">
                    </div>
                    <div class="form-group">
                        <label for="off_address">Office Address</label>
                        <input type="text" name="off_address" id="off_address" class="form-control" maxlength="191" value="{{ get_center_details('off_address', 'off_address') }}">
                    </div>
                    <div class="form-group">
                        <label for="contact_no">Phone No.</label>
                        <input type="text" name="contact_no" id="contact_no" class="form-control" maxlength="10" value="{{ get_center_details('contact_no', 'contact_no') }}">
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label for="running_time">Running Since</label>
                            <input type="date" name="running_time" id="running_time" class="form-control" value="{{ get_center_details('running_time', 'running_time') }}">
                        </div>
                        <div class="col-md-6">
                            <label for="no_students">Student Strength</label>
                            <input type="text" name="no_students" id="no_students" class="form-control" maxlength="6" value="{{ get_center_details('no_students', 'no_students') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="running_courses">Running Courses</label>
                        <textarea name="running_courses" id="running_courses" class="form-control" maxlength="191">{{ get_center_details('running_courses', 'running_courses') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="lab_mat">Lab Materials</label>
                        <textarea name="lab_mat" id="lab_mat" class="form-control" maxlength="191">{{ get_center_details('lab_mat', 'lab_mat') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="ownership">Ownership/Partnership Details</label>
                        <input type="text" name="ownership" id="ownership" class="form-control" maxlength="191" value="{{ get_center_details('ownership', 'ownership') }}">
                    </div>
                    
                    {{-- From Mthod --}}
                    <input type="hidden" name="_method" value="PUT">
                  </div>
                  {{-- /Card Body --}}
                  {{-- Header --}}
                  <div class="card-header">
                        <h3 class="card-title">Fill Up Personal Details</h3>
                  </div>
                  {{-- Card Body --}}
                  <div class="card-body">
                    <div class="form-group">
                        <label for="f_name">Father's Name</label>
                        <input type="text" name="f_name" id="f_name" class="form-control" maxlength="191" value="{{ get_center_details('f_name', 'f_name') }}">
                    </div>
                    <div class="form-group">
                        <label for="res_address">Residential Address</label>
                        <input type="text" name="res_address" id="res_address" class="form-control" maxlength="191" value="{{ get_center_details('res_address', 'res_address') }}">
                    </div>
                    <div class="form-group">
                        <label for="dob">Date Of Birth</label>
                        <input type="date" name="dob" id="dob" class="form-control" value="{{ get_center_details('dob', 'dob') }}">
                    </div>
                    <div class="form-group">
                        <label for="qualification">Last Qualification</label>
                        <input type="text" name="qualification" id="qualification" class="form-control" maxlength="52" value="{{ get_center_details('qualification', 'qualification') }}">
                    </div>
                    <div class="form-group">
                        <label for="aadhar_no">Aadhar No.</label>
                        <input type="text" name="aadhar_no" id="aadhar_no" class="form-control" maxlength="12" value="{{ get_center_details('aadhar_no', 'aadhar_no') }}">
                    </div>
                    <div class="form-group">
                        <label for="pan_no">Pan No.</label>
                        <input type="text" name="pan_no" id="pan_no" class="form-control" maxlength="10" value="{{ get_center_details('pan_no', 'pan_no') }}">
                    </div>
                  </div>
                  {{-- Header --}}
                  <div class="card-header">
                      <h3 class="card-title">Documents</h3>
                  </div>
                  {{-- Card Body --}}
                  <div class="card-body">
                    <div class="form-group">
                      <div class="custom-file">
                          <input type="file" name="center_img" id="center_img" class="form-control">
                          <label for="center_img" class="custom-file-label">Upload / Update Center Image (Maximum File Size 150 KB)</label>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="custom-file">
                          <input type="file" name="trade_licence_img" id="trade_licence_img" class="form-control">
                          <label for="trade_licence_img" class="custom-file-label">Upload / Update Trade Licence Image (Maximum File Size 150 KB)</label>
                      </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" name="director_img" id="director_img" class="form-control">
                            <label for="director_img" class="custom-file-label">Upload / Update Director Image (Maximum File Size 50 KB)</label>
                        </div>
                      </div>
                  </div>

                  {{-- Card Footer --}}
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary btn-flat">Save Changes</button>
                      <a href="#" class="btn btn-danger btn-flat" onclick="event.preventDefault();
                      document.getElementById('submit-form').submit();">Submit Now</a>
                    </div>
                  {{-- /Card Footer --}}
                </form>
                <form id="submit-form" action="{{ route('center-submit') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </div>
              @elseif(Auth::user()->confirmed_at == null)
                <div class="callout callout-success">
                  <h5>Form is Successfully Submitted</h5>

                  <p>Now your Institue is pending for approval</p>
                </div>
              @else
              <div class="callout callout-success">
                <h5>Congratulations!!!</h5>

                <p>Your Account is now active</p>
              </div>
              <div class="container-fluid">
                <div class="row">
                  <div class="col-lg-3 col-6">
                      <div class="small-box bg-warning">
                          <div class="inner">
                            <h3>{{ count(get_center_students(Auth::guard('center')->user()->id, 'pending')) }}</h3>
                            <p>Students Pending</p>
                          </div>
                          <div class="icon">
                            <i class="fas fa-user-plus"></i>
                          </div>
                          <a href="{{ route('center-students-pending') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                  </div>
                  <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                          <h3>{{ count(get_center_students(Auth::guard('center')->user()->id, 'active')) }}</h3>
                          <p>Students Active</p>
                        </div>
                        <div class="icon">
                          <i class="fas fa-smile"></i>
                        </div>
                        <a href="{{ route('center-students-active') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              @endif
            </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('inc.dashboard-footer')
@endsection
